from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import login as authlogin, authenticate, logout as authlogout
import sys
import os
import base64
from django.contrib.auth.models import User

# Loading the SDK Python file.
from . import IdcsClient
import simplejson as json

CONFIG_FILE = os.getenv('CONFIG_FILE','config.json')


def index(request):
    print >> sys.stderr, "----------------- def index(request) {} ---------------".format(request.META)
    if 'HTTP_AUTHORIZATION' in request.META:
        auth = request.META['HTTP_AUTHORIZATION'].split()

        if len(auth) == 2:
            if auth[0].lower() == "basic":
                username, password = base64.b64decode(auth[1]).split(':')
                user = authenticate(request,username=username,password=password)
                if not user or not user.is_active:
                    response = HttpResponse(status=401)
                    response['WWW-Authenticate'] = 'User not found'
                    return response
                request.user = user
                return HttpResponse(status=200)

        response = HttpResponse()
        response.status_code = 401
        response['WWW-Authenticate'] = 'Basic realm="%s"' % "Basic Auth Protected"
        return response

    access_token = request.session.get('access_token', None)



    if not access_token:
        # If the access token isn't present redirects to login page.
        return render(request, 'sampleapp/index.html', status=403)
    return HttpResponseRedirect('/home')

def login(request):
    print >> sys.stderr, "----------------- def login(request) ---------------"
    return render(request, 'sampleapp/index.html')


def about(request):
    print >> sys.stderr, "----------------- def abouth(request) ---------------"
    return render(request, 'sampleapp/about.html')


# Definition of the /auth route
def auth(request):
    print >> sys.stderr, "----------------- def auth(request) ---------------"
    # Loading the configurations
    options = getOptions()
    print  >> sys.stderr, "{0} file = {1}".format(CONFIG_FILE,options)

    # Authentication Manager loaded with the configurations.
    am = IdcsClient.AuthenticationManager(options)

    # Using Authentication Manager to generate the Authorization Code URL, passing the
    # application's callback URL as parameter, along with code value and code parameter.
    url = am.getAuthorizationCodeUrl(options["redirectURL"], options["scope"], "1234", "code")

    print >> sys.stderr, "Redirecting to URL " + str(url)

    # Redirecting the browser to the Oracle Identity Cloud Service Authorization URL.
    return HttpResponseRedirect(url)

# Definition of the /callback route
def callback(request):
    print >> sys.stderr, "----------------- def callback(request) {}---------------".format(request.META)

    code = request.GET.get('code')
    # Authentication Manager loaded with the configurations.
    am = IdcsClient.AuthenticationManager(getOptions())
    # Using the Authentication Manager to exchange the Authorization Code to an Access Token.
    ar = am.authorizationCode(code)
    # Get the access token as a variable
    access_token = ar.getAccessToken()
    id_token = ar.getIdToken()

    print >> sys.stderr, "--------"
    print >> sys.stderr, "code = %s" % (code)

    print >> sys.stderr, "--------"
    print >> sys.stderr, "access_token = %s" % (access_token)

    print >> sys.stderr, "--------"
    print >> sys.stderr, "id_token = %s" % (id_token)

    # Validating id token to acquire information such as UserID, DisplayName, list of groups and AppRoles assigned to the user
    id_token_verified = am.verifyIdToken(id_token)

    print >> sys.stderr, "--------"
    print >> sys.stderr, "id_token_verified = %s" % (id_token_verified)

    displayname = id_token_verified.getDisplayName()

    # The application then adds these information to the User Session.
    request.session['access_token'] = access_token
    request.session['id_token'] = id_token
    request.session['displayname'] = displayname

    username = displayname.title().replace(' ','.')
    pw = access_token[:20]

    user, created = User.objects.get_or_create(username=username)
    user.set_password(pw)
    user.save()

    print >> sys.stderr, "user = {} password = {} new = {}".format(username, pw, created)

    options = am.options
    if 'nextURL' in options:
        return HttpResponseRedirect(options['nextURL'])

    # Rendering the home page and adding displayname to be printed in the page.
    return HttpResponseRedirect('/home')

# Definition of the /home route
def home(request):
    print >> sys.stderr, "----------------- def home(request) ---------------"
    access_token = request.session.get('access_token', 'none')
    if access_token ==  'none':
        return HttpResponseRedirect('/login')
    else:
        options = getOptions()
        displayname = request.session.get('displayname', 'displayname')
        return render(request, 'sampleapp/home.html', {'displayname': displayname,
                                                       'baseurl':options['BaseUrl'],
                                                       'appid':options['AppID']}
                      )

# Definition of the /myProfile route
def myProfile(request):
    print >> sys.stderr, "----------------- def myProfile(request) ---------------"
    # Getting the Access Token value from the session
    access_token = request.session.get('access_token', None)
    if not access_token:
        # If the access token isn't present redirects to login page.
        return HttpResponseRedirect('/login')
    else:
        # If the access token is present,  validates the id token to acquire
        #   information such as UserID, DisplayName, list of groups and AppRoles assigned to the user.

        # Authentication Manager loaded with the configurations.
        am = IdcsClient.AuthenticationManager(getOptions())
        id_token = request.session.get('id_token', 'none')
        id_token_verified = am.verifyIdToken(id_token)

        # Getting the user details in json format.
        jsonProfile = id_token_verified.getIdToken()

        # Getting User information to send to the My Profile page.
        displayname = request.session.get('displayname', 'displayname')
        
        # Rendering json to be used in the html page.
        json_pretty = json.dumps(jsonProfile, sort_keys=True, indent=2)
        context = {
            'displayname': displayname,
            "json_pretty": json_pretty,
        }

        first, second, last = access_token.split('.')
        import base64
        print >> sys.stderr, "decoded first = %s" % decode64(first)
        print >> sys.stderr, "decoded second = %s" % decode64(second)

        # Rendering the content of the My Profile Page.
        return render(request, 'sampleapp/myProfile.html', context)

def decode64(s):
    s += '=' * (-len(s) % 4)

    return s.decode('base64')

# Definition of the /logout route
def logout(request):

    print >> sys.stderr, "----------------- def logout(request) ---------------"

    # Getting the Access Token value from the session
    access_token = request.session.get('access_token', None)
    if not access_token:

        # If the access token isn't present redirects to login page.
        return HttpResponseRedirect('/login')
    else:
        options = getOptions()
        url = options["BaseUrl"]
        url += options["logoutSufix"]
        url += '?post_logout_redirect_uri=http%3A//localhost%3A8000&id_token_hint='
        url += request.session.get('id_token', 'none')

        # Clear session attributes
        del request.session['access_token']
        del request.session['id_token']

        name = request.session['displayname']
        del request.session['displayname']
        authlogout(request)
        try:
            u = User.objects.get(username=name)
            u.delete()
            print >> sys.stderr, "----deleted iuser {}-----".format(u)

        except User.DoesNotExist:
            pass

        # Redirect to Oracle Identity Cloud Service logout URL.
        print >> sys.stderr, "logout URL: {}".format(url)
        return HttpResponseRedirect(url)

# Function used to load the configurations from the config.json file or env
def getOptions():

    options = {}
    if os.path.exists(CONFIG_FILE):
        fo = open(CONFIG_FILE, "r")
        config = fo.read()
        options = json.loads(config)
        fo.close()
    options['ClientId'] = options['ClientId'] or os.getenv('CLIENT_ID','')
    options['ClientSecret'] = options['ClientSecret'] or os.getenv('CLIENT_SECRET','')
    options['BaseUrl'] = options['BaseUrl'] or os.getenv('BASE_URL','')
    options['AudienceServiceUrl'] = options['AudienceServiceUrl'] or os.getenv('BASE_URL','')
    options['scope'] = options['scope'] or os.getenv('SCOPE','urn:opc:idm:t.user.me openid')
    options['redirectURL'] = options['redirectURL'] or os.getenv('CALLBACK_URL',"http://localhost:8000/callback")
    options['nextURL'] = options['nextURL'] or os.getenv('NEXT_URL','/home')
    options['logoutSufix'] = options['logoutSufix'] or os.getenv('LOGOUT_SUFIX',"/")
    options['LogLevel'] = options['LogLevel'] or os.getenv('LOG_LEVEL','DEBUG')
    options['AppID'] = options['AppID'] or os.getenv('APP_ID','')
    return options
